use crate::misc::{read_lines, string_to_integer};

pub(crate) fn day_one_ctrl(){
    let test_file = "input/day01/test.txt";
    let input_file = "input/day01/input.txt";
    println!("Day 1:");
    println!("Problem 1:");
    println!("Test solution = {}", part_one(test_file));
    println!("Input solution = {}", part_one(input_file));
    println!("\nProblem 2:");
    println!("Test Solution: {}", part_two(test_file));
    println!("Input Solution: {}", part_two(input_file));
}

fn part_one(mut file_path:&str) -> i32{
    let mut result = 0;
    let values_string = read_lines(file_path);
    let values = string_to_integer(values_string);
    let mut i = 0;
    let mut j = 1;
    while i < values.len() {
        j = i + 1;
        while j <values.len() {
            if (values.get(i).unwrap() + values.get(j).unwrap()) == 2020{
                break;
            }
            j += 1;
        }
        if j != values.len(){
            break;
        } else {
            i += 1;
        }
    }

    result = values.get(i).unwrap() * values.get(j).unwrap();
    return result;
}

fn part_two(mut file_path:&str) -> i32{
    let mut result = 0;
    let values_string = read_lines(file_path);
    let values = string_to_integer(values_string);
    let mut i = 0;
    let mut j = 1;
    let mut k = 2;
    let mut l = 0;
    while i < values.len() {
        j = i + 1;
        if j == 1{
            k = 2;
        } else {
            k = 1;
        }
        while j < values.len() {
            l = k;
            while k < values.len() {
                if (!(j == k)) && ((values.get(i).unwrap() + values.get(j).unwrap() + values.get(k).unwrap()) == 2020){
                    break;
                }
                k += 1;
            }
            if k < values.len() {
                break;
            } else {
                k = l;
                j += 1;
            }
        }
        if j != values.len(){
            break;
        } else {
            k = l;
            i += 1;
        }
    }

    result = values.get(i).unwrap() * values.get(j).unwrap() * values.get(k).unwrap();
    return result;
}