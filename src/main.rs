mod misc;
mod day01;

use std::env;
use std::time;
use crate::day01::day_one_ctrl;

fn main() {
    let args: Vec<String> = env::args().collect();
    let arg = &args[1];
    let day: i32 = arg.parse().unwrap();
    let before = time::Instant::now();
    match day{
        1 => day_one_ctrl(),
        _ => print!("Please enter a number between 1 and 25")
    }
    println!("It took {:.2?} to complete", before.elapsed());
}
