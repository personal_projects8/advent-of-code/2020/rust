use std::fs;

pub(crate) fn string_to_integer(strings: Vec<String>) -> Vec<i32>{
    let mut result = Vec::new();
    for s in strings {
        result.push(s.parse().unwrap());
    }
    return result;
}

pub(crate) fn read_lines(file_path: &str) -> Vec<String> {
    let mut result = Vec::new();
    for line in fs::read_to_string(file_path).unwrap().lines() {
        result.push(line.to_string());
    }
    return result;
}